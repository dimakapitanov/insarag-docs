.. _volume2-manual_b-body:

==================
Vol II. Man B
==================

Abbreviations
============================

Introduction
=============

=====================================
1. International USAR Response Cycle
=====================================

==============================================================
2. Roles and Responsibilities in International USAR Response
==============================================================

2.1. United Nations Offices
============================

2.2. United Nations Monitoring and Disaster Alert Systems
=========================================================

2.3. Affected Countries
========================
