.. _volume2-manual_a-body:

===============================================
Vol II. Man A
===============================================

Chapeau
===============================================

Abbreviations
============================

Introduction
=============

=======================================
Part 1: Building Local Capacity
=======================================

====================
1. First Responder
====================

=================================
2. Technical Rescue Capabilities
=================================

============================================
3. Considerations Before Forming a Team
============================================

Is a team needed in our community?
====================================

What type of team is needed for our community?
==============================================

Do we have commitment from the organisation membership for this?
=================================================================

=======================================
4. How to Form a Technical Rescue Team
=======================================

