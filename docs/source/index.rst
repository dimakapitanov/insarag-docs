.. _index:

=================================
INSARAG Guidelines ver. |version|
=================================

Гайдлайны INSARAG в удобочитаемом формате

<-- Оглавление тут

.. toctree::
   :caption: Vol I: Policy
   :hidden:

   volume1/body

.. toctree::
   :caption: Vol II. Man A: Capacity Building
   :hidden:

   volume2/manual_a/body

.. toctree::
   :caption: Vol II. Man B: Operations
   :hidden:

   volume2/manual_b/body

