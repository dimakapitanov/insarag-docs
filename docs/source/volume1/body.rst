.. _volume1-body:

=========================
Volume I
=========================

Abbreviations
=================

:FCSS: Field Coordination Support Section

:FMTs: Foreign MedicaHelicopters for rapid air assessments by the UNDAC team anl Teams
:GDACS: Global Disaster Alert and Coordination System
:Hazmat: Hazardous materials
:HCT: Humanitarian Country Team
:IEC: INSARAG External Classification
:IER: INSARAG External Reclassification
:INSARAG: The International Search and Rescue Advisory Group
:NGOs: Non-governmental organisations
:LEMA: Local Emergency Management Authority
:OCHA: United Nations Office for the Coordination of Humanitarian Affairs OSOCC On-Site Operations Coordination Centre
:RDC: Reception/Departure Centre
:UCC: USAR Coordination Cell
:UN: United Nations
:UNDAC: United Nations Disaster Assessment and Coordination team USAR Urban search and rescue
:VO: Virtual On-Site Operations Coordination Centre


Foreword
=========

Since its inception in 1991, the International Search and Rescue Advisory Group (INSARAG) has made a significant contribution to the strengthening of the role of the United Nations in making humanitarian coordination and response more effective, timely, and coherent. As a result, a greater number of lives are saved in the wake of disasters.
Over the past 23 years, the cooperation of the Office for the Coordination of Humanitarian Affairs (OCHA) with INSARAG has continuously been strengthened, building upon the foundation of UN General Assembly resolution 57/150 of 2002 on “Strengthening the effectiveness and coordination of international urban search and rescue assistance.”
To ensure that INSARAG is fit-for-purpose, the network has revised the INSARAG Guidelines based on lessons learnt and the sharing of best practices amongst its members. The new Guidelines help ensure high quality support in the critical life-saving activity of search and rescue in the immediate aftermath of a disaster.
We look forward to your continued collaboration. Our partnership and the support of Member States are essential to the success of the INSARAG network.

Valerie Amos

Under-Secretary-General for Humanitarian Affairs and Emergency Relief Coordinator

February 2015


Introduction
===============

The INSARAG Guidelines comprise three volumes: Volume I: Policy; Volume II: Preparedness and Response; and Volume III: Operational Field Guide.

This is the INSARAG Guidelines, Volume I: Policy. This volume describes the INSARAG methodology for international urban search and rescue (USAR) operations and the policy that underpins it. In particular, it describes:
* INSARAG and how it operates
* The roles of affected countries and those assisting in international USAR responses
* Building national USAR capacity
* The INSARAG External Classification (IEC) and INSARAG External Reclassification (IER) systems

This volume is targeted at the sponsoring organisations’ person nominated to be a country’s Policy Focal Point – the one who represents the country’s capabilities in providing or receiving USAR assistance –, and at others involved in providing humanitarian assistance at a policy- or decision-making level.

The three volumes describe the INSARAG methodology. The other two volumes are as follows:

    **Volume II: Preparedness and Response**
        * Manual A: Capacity Building
        * Manual B: Operations
        * Manual C: INSARAG External Classification and Reclassification

These manuals provide guidance and procedures on USAR methodology and explain the minimum standards and procedures for building up a USAR team, as well as on training, readiness, classification and operations. Volume II is based on the required capabilities. It is targeted at the person nominated as the Operational Focal Point of the INSARAG member country, as well as at the focal point of a USAR team and a USAR team’s management.

    **Volume III: Operational Field Guide**

This pocket-size volume provides field and tactical information at a glance, and ought to be carried by all responding USAR team members in trainings and missions.

.. note::
    The Guidelines can be downloaded from www.insarag.org. Hard copies in English (and translated versions where available) can be requested from the INSARAG Secretariat by email on insarag@un.org. Supplementary guidance notes on technical issues developed by INSARAG Working Groups and relevant institutions are available from www.insarag.org.

Purpose of the INSARAG Guidelines
-------------------------------------

This internationally accepted document provides a methodology to guide countries affected by a sudden-onset disaster causing large-scale structural collapse, as well as international USAR teams responding in the affected country. The guidelines also outline the role of the UN in assisting affected countries in on-site coordination.

The methodology, as defined in the INSARAG Guidelines, provides a process for preparedness, cooperation and coordination of the national and international participants. This should then result in an improved understanding at all government levels in the affected country as to how international USAR assistance can be utilised to augment the national response so to ensure the most effective use of resources.

===========
1. INSARAG
===========

1.1. What is INSARAG?
=======================

INSARAG was created in 1991 following the initiatives of the specialised international USAR teams who operated together in the Mexican earthquake of 1985 and Armenian earthquake of 1988. INSARAG is an inter-governmental humanitarian network of disaster managers, government officials, non-governmental organisations (NGOs) and USAR practitioners operating under the umbrella of the UN, and within the realm of its mandate contributes to the implementation of the International Strategy for Disaster Reduction (ISDR).

INSARAG successfully achieved the adoption of a UN General Assembly Resolution, GA57/150 in 2002 on "Strengthening the Effectiveness and Coordination of International USAR Assistance" (http://www.insarag.org/en/about/ga-resolution.html). This resolution has underpinned much of the progress achieved by INSARAG.

1.2. Vision and Role
========================

INSARAG’s vision is to save lives by promoting efficiency, enhanced quality, and coordination amongst national and international USAR teams on the basis of adherence to common guidelines and methodologies.

INSARAG’s role is to prepare for, mobilise and coordinate effective and principled international USAR assistance in support of an affected country in collapsed-structure emergencies and to support capacity-building at the international, regional, sub-regional and national level. INSARAG does this by:

* Developing and promoting common standards for USAR assistance, coordination methodologies and tools, and mobilisation and information exchange protocols between relevant stakeholders
* Promoting cooperation and experience-sharing amongst, and in partnership with, Member States, NGOs, and national, regional and international partners

The INSARAG Hyogo Declaration of 2010 (http://insarag.org/en/about/mandate-of-insarag-leftmenu.html) on the “Recognition and strengthening of international urban search and rescue operational standards,” adopted in the first INSARAG Global Meeting, gave INSARAG a renewed impetus and guidance for its work, and emphasised on the need for strengthening of national response capacities and highly recommends that building national, local and community capacity is critical for effective response.

1.3. Mandate of INSARAG
=========================

INSARAG is mandated by the INSARAG Steering Group to:

* Render emergency preparedness and response activities more effective and thereby save more lives, reduce suffering and minimise adverse consequences
* Improve efficiency in cooperation among international USAR teams working in collapsed structures at a disaster site, including by managing the INSARAG External Classification (IEC) process
* Promote the strengthening of national USAR capacities and activities designed to improve search-and- rescue preparedness in disaster-prone countries, thereby prioritising developing countries, including by assisting countries in setting up national USAR team classification processes
* Develop internationally accepted procedures and systems for sustained cooperation between national USAR teams operating on the international level
* Develop USAR procedures, guidelines and best practices, and strengthen cooperation between interested organisations during the emergency relief phase

1.4. Values, Operational Norms and Humanitarian Principles
===========================================================

*Adherence to common standards and methodology:* Members of INSARAG commit to adhere to the INSARAG Guidelines and methodology as globally-accepted and independently verifiable minimum operational standards and procedures, based upon expert knowledge and long experience. The INSARAG network continues to develop these standards and procedures though shared and continued learning.

*Inclusiveness:* INSARAG brings together governments, governmental organisations, NGOs and disaster preparedness and response professionals. INSARAG particularly encourages disaster-prone countries to join the network, as well as any country or organisation with USAR response capacity. INSARAG emphasises the importance for gender awareness and considerations while working in disaster-affected areas.

*Professionalism:* INSARAG promotes responsible, ethical and professional standards amongst USAR teams and stakeholders.

*Respect for diversity:* INSARAG acknowledges and respects USAR teams’ varied operational procedures in achieving common objectives, while disseminating principles and minimum standards agreed upon by the INSARAG network.

*Cultural sensitivity:* INSARAG promotes awareness and respect by international USAR teams of cultural differences so that international USAR teams can cooperate more effectively with national and international actors.

*Needs-driven:* Mobilisation and deployment of international USAR teams is only supported when the affected country’s capacities are overwhelmed by the impact of a collapsed-structure emergency and national authorities agree to accept international assistance. Moreover, the type of international assistance rendered is based on the needs of the affected country and not driven by the availability of resources.

*Coordination:* INSARAG promotes internationally agreed coordination structures managed and advocated by OCHA, promotes coordination of preparedness and capacity building activities, and, throughout an operation, assists countries in coordinating the emergency response.

INSARAG operates in accordance with the Humanitarian Principles, which form the core of humanitarian action. Please see https://ochanet.unocha.org/p/Documents/OOM_HumPrinciple_English.pdf for more details.

1.5. Structure and Working Process
=====================================

1.5.1. INSARAG Structure
----------------------------

INSARAG is composed of a Steering Group, three Regional Groups, and the Secretariat, as well as the USAR Team Leaders Meeting, and the Working Groups (see Figure 1). The decision-making process is described in Figure 2. This structure provides the framework for decision-making and associated processes as approved by the Steering Group in 2013.

This structure ensures that INSARAG’s aims can be achieved at a regional level, whilst ensuring full ownership and that objectives are in line with best practices as defined and agreed by the global network.

[pic] Figure 1: The INSARAG organisational structure.

[pic] Figure 2: The INSARAG Decision-Making Process

1.5.2. INSARAG Steering Group
-----------------------------
The Steering Group, supported by the Secretariat, develops policy and is responsible for providing the strategic direction of INSARAG.

The Steering Group is composed of the Global Chair, the three Regional Group Chairs and Vice-Chairs, the Working Groups (usually the Chairperson), the Secretariat, and the International Federation of Red Cross and Red Crescent Societies. Policy Focal Points of member countries with IEC classified teams, including representatives from classified NGO teams, and the USAR Team Leaders participate.

The groups that fall under the Steering Group (see Figure 1) are:

* The three Regional Groups
* The USAR Team Leaders
* The Working Groups

1.5.3. INSARAG Secretariat
---------------------------
The INSARAG Secretariat is housed within the OCHA, which is the department in the UN Secretariat mandated to mobilise and coordinate multilateral humanitarian action in response to emergencies. Within OCHA, the INSARAG Secretariat is located in the Field Coordination Support Section (FCSS) of the Emergency Services Branch of OCHA-Geneva. FCSS also manages the United Nations Disaster Assessment and Coordination (UNDAC) mechanism.

The Secretariat serves as a direct link between the Global and Regional Chairs, the INSARAG Focal Points, the USAR teams and the INSARAG network. It co-organises all INSARAG meetings, including the Regional Group meetings, workshops, IEC and IER exercises for USAR teams, and training events, in cooperation with host countries.

The Secretariat is also responsible for the management and maintenance of the INSARAG website (http://www.insarag.org). This includes the USAR Directory of INSARAG members and their teams.

The Secretariat also facilitates any relevant projects that have been agreed upon and launched by the INSARAG network.

1.5.4.INSARAG Regional Groups
---------------------------------
The three INSARAG Regional Groups are:
* The Africa-Europe-Middle East region
* The Americas region
* The Asia-Pacific region

[pic] Figure 3: The INSARAG Regional Groups

These Regional Groups meet annually to take measures to strengthen national and regional disaster preparedness, and USAR response. The Regional Groups work to ensure that the strategic direction and policies from the Steering Group are implemented, and to assimilate relevant information from participating countries for submission back to the Steering Group.

Each Regional Group is governed by a troika system where there is a Chairperson and two Vice-Chairpersons and made up of the incoming chair and former outgoing chair. They have a one-year tenure and represent the region at the Steering Group. Countries and organisations are represented in the Regional Groups through their INSARAG Policy and Operational Focal Points.

The Regional Groups are responsible for the implementation of the Steering Group decisions at the regional level, as well as for carrying out the regional annual work programme and activities planned for the region. Together with the Secretariat, they work closely with OCHA Regional and Country Offices to ensure synergies with OCHA’s plans and priorities for the region. They also endorse the creation of sub-regional groups of collaborative partners as relevant.

The sub-regional groups of collaborative partners are initiated in regions where their establishment – due to geographical, cultural and language commonalities – ensures effective implementation of the INSARAG mandate.

Since 2010 in Kobe, Japan, and once every five years, all regional groups come together in the INSARAG Global Meeting where the network convenes with the objective of strengthening the global network, thereby ensuring that it is fit for purpose in today’s rapidly changing world.

1.5.5. INSARAG Working Groups
------------------------------

Task-specific Working Groups may be established when needed at the request of the Steering Group, the Regional Groups, or the USAR Team Leaders, and endorsed by the Steering Group. Their purpose is to develop solutions to specific technical issues. They could also be tasked with the preparation and development of training packages for relevant trainings and exercises, such as the INSARAG earthquake response simulation exercise.

Each Working Group has a Chair, plus two or three members nominated from each region to ensure a full, worldwide perspective on operational and training issues raised by the USAR Team Leaders Meetings.

It may co-opt suitable USAR team members who have the relevant experience and qualifications to address the particular issue under discussion. The Secretariat facilitates the selection of these groups, assists in establishing the terms of reference, provides guidance, and establishes timelines for work completion.

The Working Groups ceases when they complete their assigned tasks. Extensions of the Working Group beyond the given mandate are decided by the Steering Group at its annual meeting in Geneva.

1.5.6. INSARAG USAR Team Leaders
------------------------------------
The USAR Team Leaders are a network of experienced national and international USAR practitioners who respond to collapsed structure incidents and other disasters as appropriate. It is composed of USAR Team Leaders and INSARAG Operational Focal Points from member countries.

This network is also called upon for nominations to participate in the Working Groups, engage in other INSARAG activities, including capacity building, and to contribute to the continued development of INSARAG as a whole.

This expert group meets annually at the INSARAG Team Leaders Meeting to share and discuss best practices, technical ideas and operational issues. The USAR Team Leaders’ inputs, advice and experience serve to improve the operational capabilities of the INSARAG methodology for both national and international USAR response. Team Leaders are encouraged to constantly exchange technical information and best practices bilaterally and sharing through the VO.

1.6. Membership
=================

INSARAG Membership is open to all UN Member States, NGOs and organisations involved in USAR activities, and upon recommendation and approval by their respective governments. INSARAG also maintains close cooperation with regional mechanisms.

INSARAG members are invited to meetings of the relevant INSARAG Regional Group and USAR Team Leaders, and to participate in the Working Groups which are made up of suitable experts nominated by the Team Leaders and Regional Groups and are supported by their respective sponsoring organisations.

Members have access to INSARAG information and knowledge-sharing tools through the INSARAG website, and through disaster alert and information sharing platforms such as the Global Disaster Alert and Coordination System (GDACS), which includes the Virtual On-Site Operations Coordination Centre (VO).

Member States with USAR teams deploying internationally are encouraged to undertake INSARAG External classification, however, this is not a requirement to be a member of the INSARAG network. As a first step, teams are encouraged to undertake national classification.

1.6.1.Requirements
------------------
To improve preparedness and response, INSARAG members share information and best practices with other INSARAG members and USAR teams, including teams that are developing a response capability or are preparing for classification.

INSARAG members are encouraged to actively participate in, and contribute to, INSARAG Regional Meetings, earthquake response simulation exercises, and other INSARAG forums such as the USAR Team Leaders Meetings, to contribute to the Working Groups, and also to support by providing technical experts to other INSARAG initiatives such as capacity assessment missions and regional exercises.

1.6.2. Policy and Operational Focal Points
----------------------------------------------
It is recommended that all Member States participating in INSARAG designate policy and operational points-of- contact for appropriate and effective information exchange, in accordance with the respective national disaster management structure. In the preparedness and response phases, INSARAG Focal Points serve as the primary link and information channel between the country and the INSARAG network, which is particularly important in response to emergencies so as to have an effective information flow between the affected country and the potential international responders. Regional, intergovernmental and international organisations participating in INSARAG are also encouraged to designate policy and operational focal points.

The Policy Focal Point is to be the central point-of-contact between the Secretariat, the INSARAG community, and all organisations in that country involved in the INSARAG network. This person represents the country’s capabilities in providing or receiving USAR assistance, assisted for operational matters by the operational focal point who is normally a USAR professional.

The Policy Focal Point normally sits in the central institution or agency of the national disaster management structure or in the agencies responsible for international cooperation and humanitarian response, and represents the country on USAR policy matters in the Regional Group and, as appropriate, in the Steering Group.

The Operational Focal Point should normally have USAR responsibilities as part of their daily functions and, by consequence, is recommended to be a USAR specialist. They represent the country primarily on operational USAR matters in INSARAG meetings, workshops, and events.

The responsibilities of INSARAG Focal Points can be described as ensuring the efficient information exchange and validation at the appropriate levels in the preparedness and response phases on USAR matters, including capacity building, trainings, policy matters, emergency alerts, requests or acceptance of assistance, mobilisation and provision of international assistance. For the annual budget planning process, Focal Points have to take into account costs for participation and engagement in supporting INSARAG events and the workplan.

The designation of INSARAG Focal Points is at the discretion of the government, in line with its respective disaster management structure and serve as a point-of-contact between the national government and the INSARAG network, including the INSARAG Secretariat, and the Regional and Steering Groups. Member States are requested to inform the INSARAG Secretariat on the designation of their INSARAG Focal Points and update this information whenever Focal Points change.

See Annex A on more detailed terms of reference of INSARAG Focal Points.

1.6.3.INSARAG Website and USAR Directory
--------------------------------------------

The INSARAG website shares information on INSARAG generally, as well as the summaries from previous events and upcoming activities.

The INSARAG USAR Directory is a unique database with the details of all INSARAG member countries and organisations and their USAR teams. The directory also contains contact details for relevant Policy and Operational Focal Points.

The directory categorises USAR teams into the following:

* International: Medium or Heavy IEC teams
* National: Light, Medium, or Heavy national teams
* Governmental and NGO teams not yet classified

In order to be part of the USAR Directory, teams need the endorsement of their country’s Policy Focal Point. Teams can request registration via their Policy Focal Point through the Secretariat. Once registered, the country’s Operational Focal Point can update the team’s entry.

.. note::
    The USAR Directory can be accessed at http://www.insarag.org.

=========================================
2. National USAR Capacity Building
=========================================


When a disaster strikes, people look first to their own communities and governments for help, and second, to neighbouring countries and regional/international organisations. International aid is the third tier of humanitarian assistance, which is called in for specialised tasks such as complex search and rescue after an earthquake.

UN General Assembly Resolution 57/150 recommends that countries build up strong national USAR response capacities to deal with any eventuality as the first step. The Resolution identifies that each country has a responsibility first and foremost to take care of the victims of natural disasters and other emergencies occurring in its own territory. Countries must be able to initiate, organise, coordinate and provide humanitarian assistance in their own territories if required.

Furthermore, the Resolution “Encourages the strengthening of cooperation among States at the regional and sub- regional levels in the field of disaster preparedness and response, with particular respect to capacity building at all levels.”

It is thus essential that countries have an effective and sustainable national USAR capability and a national crisis management system first, before developing an internationally-deployable capability, i.e. an IEC team.

The following paragraphs describe INSARAG’s guidance to Member States on building their national USAR capacity. This will be further elaborated in Volume II, Manual A: Capacity Building.

2.1. USAR Response Framework
==============================

The USAR response framework (see Figure 4) recognises that search and rescue efforts are chronological and continuous, starting immediately after the occurrence of a large-scale structural collapse disaster.

Rescue efforts start immediately with passers-by rushing in to assist. Within minutes local emergency services respond. The rescue efforts continue with the arrival of regional or national rescue resources within hours. International rescue teams respond in the days after the event and following an official request by the affected government for international assistance.

[pic] Figure 4: The INSARAG USAR response framework

The INSARAG response framework represents all levels of response, starting with spontaneous community actions immediately following the disaster, supplemented initially by the local emergency services and then by national rescue teams. The response of international USAR teams supports national rescue efforts.

Each new level of response increases the rescue capability and overall capacity but has to integrate with, and support, the response already working at the disaster.

In order to ensure interoperability between the different levels of USAR response, it is vital that working practices, technical language and information are common and shared through all levels of the USAR response framework.

Important note: Member States are strongly encouraged, as a first step, to conduct a self-assessment of their USAR response capacity based on the INSARAG Capacity Assessment Checklist for National USAR Teams – see Volume II, Manual A: Capacity Building.

2.2. USAR Capacity Building
============================
USAR capacity building is the process of developing a robust and sustainable disaster management framework with a USAR capability. Countries should have the ability to effectively use their own capability and to integrate international assets into the national response.

Capacity building should cover all five components of USAR capability; that is, management, search, rescue, medical and logistics.

It is recommended that countries seeking USAR capacity building should follow the USAR development cycle (see Figure 5).

[Figure 5: The USAR development cycle.]

2.3. USAR Capacity Assessment Missions
=========================================
In order to support countries and organisations in the process of national USAR capacity building, the INSARAG Secretariat, when requested by the relevant government, can facilitate an INSARAG USAR Capacity Assessment Mission. This would be coordinated by the INSARAG Secretariat between the requesting country and USAR experts from the INSARAG network sponsored by their governments/organisations.

A USAR Capacity Assessment Mission aims to identify existing capacities and determine the required capacities according to the country’s USAR objectives and needs. This provides an indication of the gaps between the current capacity and the required capacity, which in turn assists in tailoring the initiatives to be employed in developing USAR capacity.

The INSARAG network provides unique access to a pool of experienced and qualified USAR experts that are able to conduct an assessment of existing capacity, mapped against needs, and who are then able to provide recommendations on the subsequent implementation of USAR capacity development initiatives.

For more information on building national USAR capacity, please refer to Volume II, Manual A: Capacity Building.

=================================
3. International USAR Operations
=================================

3.1. USAR
===========
USAR involves the location, extrication, and initial stabilisation of people trapped in a confined space or under debris due to a sudden-onset large-scale structural collapse such as an earthquake, in a coordinated and standardised fashion. This can occur due to natural disasters, landslides, accidents, and deliberate actions.

The goal of search and rescue operations is to rescue the greatest number of trapped people in the shortest amount of time, while minimising the risk to rescuers.

3.2. International USAR Response Cycle
=======================================

An international USAR response has the following phases, known as USAR response cycle:

* Phase I – Preparedness: The preparedness phase is the period between disaster responses. In this phase USAR teams conduct training and exercises, review lessons learned from previous experiences, update standard operating procedures, and plan future responses
* Phase II – Mobilisation: The mobilisation phase is the period immediately following the occurrence of a disaster. International USAR teams prepare to respond and travel to deploy and assist the affected country requesting international assistance
* Phase III – Operations: The operations phase is the period when international USAR teams are performing USAR operations in the affected country. In this phase international USAR teams arrive at the Reception/Departure Centre (RDC) in the affected country, register with the On-Site Operations Coordination Centre (OSOCC), and conduct USAR operations in line with the operational objectives of the Local Emergency Management Authority (LEMA). This phase ends when the USAR team is instructed to cease USAR operations
* Phase IV – Demobilisation: The demobilisation phase is the period when international USAR teams have been instructed that USAR operations are to cease. USAR teams commence withdrawal, coordinating their departure through the OSOCC, and then depart from the affected country through the RDC
* Phase V – Post-Mission: The post-mission phase is the period immediately after a USAR team has returned home. In this phase the USAR team is required to complete and submit a post-mission report and conduct an after-action review in order to improve the overall effectiveness and efficiency for response to future disasters. Figure 6 illustrates the INSARAG international USAR response cycle

[Figure 6: The INSARAG International USAR Response Cycle.]

3.3. Stakeholders and Operational Actors
=========================================

3.3.1. Affected Countries
-------------------------

Affected countries are those that experiencing a sudden-onset disaster that may require international USAR assistance. They must undertake a number of activities throughout the response cycle.

In disasters such as earthquakes, reaching trapped and injured victims quickly is the top priority in successful life- saving rescue operations. Potentially affected countries are encouraged to have a national disaster response mechanism in place such that in the first hours they are able, through their initial response and assessments, to make a decision and announce whether or not the situation is overwhelming, and, therefore, warrants immediate support from international USAR teams.

The INSARAG Hyogo Declaration of 2010 “... invites countries affected by disasters to consider the specific assistance of INSARAG IEC teams to respond by offering priority access to such teams that will make a genuine and meaningful difference in the life-saving search and rescue phase of an earthquake or other disasters involving collapsed structures.”

Affected countries can formally request assistance through their UN Resident Coordinator’s Office, the OCHA Regional or Country Office, directly through the INSARAG Secretariat or bilaterally to countries with whom it may have agreements. In the latter case, affected countries are encouraged to coordinate with and inform the INSARAG Secretariat of the response requirements.

One of the affected country’s main responsibilities is to ensure that its LEMA is functional during the disaster so as to exercise its primary role in initiating, coordinating and organising the international humanitarian assistance on their territories, and that they have overall responsibility for the command, coordination and management of the response operation.

If able, the affected country also establishes or supports the first arriving INSARAG team to establish an RDC and an OSOCC. They further conduct needs assessments and identify their priorities and where international teams can be best deployed so as to fill the gaps or augment the national rescue operations.

When international assistance is no longer required, the affected country declares the end of USAR operations, through its LEMA and after consultations with OCHA or the UNDAC team that manages the OSOCC.

Countries likely to be affected by such disasters are strongly encouraged to develop and maintain their own national first response USAR capacity according to the INSARAG Guidelines.

3.3.2. Assisting Countries: Bilateral Responders
-------------------------------------------------

Many countries, international organisations and NGOs have standby capacity (e.g. INSARAG USAR teams, Foreign Medical Teams (FMTs) that can be deployed on short notice to assist in disasters in affected countries. They may coordinate their assistance bilaterally with the affected country or through a regional organisation, such as the European Union or the Association of South East Asian Nations.

A country or organisation may also decide to channel their support through the UN agencies or NGOs. Humanitarian partners in-country normally set up a coordination process (e.g. through clusters) in support of the affected country.

Bilateral response represents the vast majority of international assistance in major disasters, which is usually managed by the authorities of the affected country. All countries are encouraged to coordinate their assistance also through the international platforms established for this purpose such as the VO and the physical OSOCC in-country as well as the specific clusters.

Assisting countries in the context of INSARAG are those with suitable USAR teams who are deploying to the affected country to provide USAR assistance to saving lives.

The INSARAG Hyogo Declaration 2010 “calls upon all urban search and rescue teams responding internationally to earthquakes to follow the field coordination procedures of OCHA, especially those laid down in the INSARAG Guidelines and Methodology and to coordinate their work with the direction of the RDC and the OSOCC established in the disaster area by the UN” and in support of the government’s overall response plan.

3.3.3. International USAR Teams
--------------------------------

INSARAG international USAR teams are response assets from the international community that respond to carry- out USAR activities in collapsed structures.

USAR teams prepare for international deployment by maintaining their capability in a state of readiness for rapid international deployment. During operations, teams perform tactical operations as required in accordance with the INSARAG Guidelines, coordinate with the OSOCC, and align their response with the priority needs of the affected country.

For more information on the USAR team functions, structure and coordination processes, please refer to Volume II, Manual B: Operations.

3.3.4. USAR Team Capabilities
---------------------------------

USAR is considered to be a multi-hazard discipline designed to respond to sudden-onset events that result in collapsed structures in an urban environment.

USAR teams conduct search and rescue operations in collapsed structures, and provide emergency medical care to trapped people. They are equipped with search tools (dogs and electronics) to find survivors. They also need to access and control utilities such as electricity and water, and detect hazardous materials (hazmat). They assess and stabilise damaged structures. Such teams are also adaptable when working in challenging environments and can support in assessments, debris removal, victim search, medical assessments/treatment.

Countries are encouraged to standardise USAR team capacities at the national level, based on their local needs and using the INSARAG Guidelines as appropriate, and establish corresponding USAR team classification processes at the national level.

USAR teams deploying internationally should also have the capability to undertake a number of activities associated with large-scale disasters and augment ongoing national rescue efforts. These include:

* Providing initial Disaster Impact Assessments
* Supporting establishment of coordination structures
* Undertaking early relief operations prior or jointly in support of other humanitarian systems

Some teams have additional resources to support relief operations – often referred to as “beyond the rubble” – with specific thematic assistance such as medical capabilities, water purification and clearing or making safe of dangerous damaged structures and debris.

If they are the first coordination resource to arrive in an affected country, these teams are also able to set up the RDC and the provisional OSOCC so as to assist the national authorities in coordinating incoming international resources.

USAR teams are expected to be self-sufficient, around the clock for 7-10 days of operational deployment and work at more than one site, depending upon the USAR team’s classification. They will establish a Base of Operations (BoO) that will support the teams for the duration of the response and serve as the communications hub for the team’s operations.

When USAR teams augment the USAR Coordination Cell (UCC) in the OSOCC with personnel, they should understand the existing LEMA coordination structure and the civil-military coordination platform in place and/or the request for assistance being facilitated/coordinated by the UN Civil-Military Focal Point in the UNDAC team. This includes being aware of potential needs that USAR teams may request from the military in case of unforeseen developments such as:

* Transport for arriving USAR teams from airport to OSOCC/Base of Operations and subsequent tasked areas of operations
* Fuel for USAR teams’ vehicles and generators
* Helicopters for rapid air assessments by the UNDAC team and partners (extent of impact, key infrastructure constraints, priority needs and areas)
* Maps, if available, for USAR team tasking
* Facilitation of immediate set up of Airport Reception Centre by the UNDAC/USAR teams
* Security support (route or area security) for USAR teams travelling to/working in insecure areas

The professional standing and conduct of INSARAG USAR teams when operating at a disaster is of prime concern to INSARAG because every member represents INSARAG. International responders need to consider the cultural, ethical, and moral differences of the country in which they are providing assistance. They must not impose any additional burden on the resources of affected countries, and can achieve this by responding in a well-prepared manner, properly trained and equipped to fully support the national authorities.

USAR teams should refer to Volume II, Manual B: Operations and Volume III, Operational Field Guide for guidance.

3.3.5. OCHA, UNDAC and LEMA
-------------------------------

OCHA serves as the INSARAG Secretariat and is mandated to coordinate international assistance in disasters and humanitarian crises exceeding the capacity of the affected country.

Many organisations, such as governments, NGOs, UN agencies, and individuals, respond to disasters and humanitarian crises. OCHA shares timely information and works with all participants to respond to disasters in such a way as to assist the government of the affected country to ensure the most effective use of international resources.

The UNDAC team is an OCHA tool used for deployment primarily to sudden-onset emergencies. OCHA dispatches an UNDAC team when requested to do so by the affected government or the UN Resident Coordinator/Humanitarian Coordinator in the affected country.

UNDAC team members are experienced emergency managers from countries, international organisations and OCHA. The UNDAC team is managed by the Field Coordination Support Section in OCHA, Geneva, and works under the authority of the UN Resident Coordinator/Humanitarian Coordinator and, where existing, the OCHA Country Office. It also works in support of, and in close cooperation with, the LEMA and the Humanitarian Country Team (HCT). The UNDAC team assists the LEMA with the coordination of international response including USAR, assessments of priority needs and information management by establishing, amongst other structures, an OSOCC and RDC, when required.

3.3.6.Reception and Departure Centre (RDC)
---------------------------------------------

The RDC is established to support the affected countries in coordinating incoming international USAR teams and other humanitarian assistance and reporting this to the LEMA through the OSOCC. It is established by the UNDAC team or the first arriving INSARAG USAR team, in collaboration with the local airport authorities. This RDC also serves as a tool to coordinate the departure of the teams in a proper manner.

3.3.7.On-Site Operations Coordination Centre (OSOCC)
----------------------------------------------------

An OSOCC is established close to the LEMA and as close to the disaster site as is safe. The OSOCC coordinates international responders in support of the LEMA, and also supports the initial inter-cluster coordination mechanisms such as health, water, sanitation, and shelter.
The OSOCC has two core objectives:

* To rapidly provide a means to facilitate on-site cooperation, coordination and information management between international responders and the government of the affected country in the absence of an alternate coordination system.
* To establish a physical space to act as a single point of service for incoming response teams, notably in the case of a sudden-onset disaster where the coordination of many international response teams is critical to ensure optimal rescue efforts.

3.3.8. USAR Coordination Cell (UCC)
-----------------------------------

The UCC is a specialised and integral part of an OSOCC during an earthquake or collapsed-structure emergency. It is established to assist and coordinate multiple international USAR teams during the operations phase of a disaster.

.. note::
    The requirements for establishing a provisional OSOCC and a UCC are detailed in Volume II, Manual B: Operations and the OSOCC Guidelines.

3.3.9. Global Disaster Alert Coordination System (GDACS)
--------------------------------------------------------

GDACS services aim at facilitating near real-time alerts, and information exchange among all actors in support of decision-making and coordination. GDACS services build on the collective knowledge of disaster managers worldwide and the joint capacity of all relevant disaster information systems.

3.3.10. Virtual On-Site Operations Coordination Centre (VO)
-------------------------------------------------------------

The VO is a web-based information management tool. It is a virtual version of the OSOCC and is part of the GDACS platform.

The VO is a crucial information sharing portal to facilitate near real-time information exchange amongst international responders and with the affected country, and the UN response mechanisms, following a sudden- onset disaster.

Access to the VO is restricted to emergency response stakeholders – registration is required. The VO is managed by the Activation and Coordination Support Unit at OCHA in Geneva.

.. note::
    USAR teams can access detailed information in Volume II, Manual B: Operations. GDACS and the VO can be accesses at www.gdacs.org and vosocc.unocha.org respectively.

More information on OCHA’s emergency disaster response systems is available at http://www.unocha.org/what-we-do/coordination/response/overview

==================================================================
4. International Classification and Reclassification of USAR Teams
==================================================================

4.1. Background
===============
Prior to the introduction of the INSARAG Classification System, USAR teams completed a self-classification as a Light, Medium or Heavy USAR team. This self-classification was then submitted to the INSARAG Secretariat and recorded in its Directory of International USAR Teams. INSARAG strongly recommends Member States to establish national USAR team classification processes as an initial step.

In 2005, the INSARAG network supported the establishment of independently verifiable, operational standards for international USAR teams through the IEC process, and encourages all Member States with USAR teams to be deployed internationally to ensure their teams take into account the IEC process.

In a world in which disaster response is becoming more complex, INSARAG has provided a commendable standard-setting model for the rest of the humanitarian community. The IEC system provides a global and strategic approach to ensure that there are well-qualified and professional teams all around the world – especially close to potential disaster prone areas – that are ready to respond at a moment’s notice, and operating upon globally accepted standards.

Affected countries will now be able to know what type of assistance they can expect to receive, and INSARAG classified USAR teams working alongside each other will be able to know the capacities each can offer: a professional response meeting the standards set in the INSARAG Guidelines, a team that speaks a common global USAR language, a team that will make a real difference in the life-saving phase of a disaster.

4.2. Categories of USAR Teams
==============================

The INSARAG Guidelines classify USAR teams according to their capacity to provide the key components of USAR. The five key components are management, search, rescue, medical, and logistics.

*Light USAR teams*

Light USAR teams have basic or minimal operational capabilities in terms of rescue equipment, knowledge and competencies, and do not necessarily all of the five key USAR components. However, Light USAR teams are usually able to assist with the surface search and rescue of victims in the immediate aftermath of a sudden-onset structural collapse disaster. Due to their limitations, Light USAR teams do not partake in the IEC process and therefore do not normally deploy internationally.

*Medium USAR teams*

A Medium USAR team comprises the five components listed above and has the ability to conduct complex technical search and rescue operations in collapsed or failed structures of heavy wood and/or reinforced masonry construction, including structures reinforced and/or built with structural steel. They must also conduct rigging and lifting operations. A Medium USAR team is expected to have the operational capability to work only at one worksite.

*Heavy USAR teams*

Heavy USAR teams comprise the five components listed above and have the operational capability for complex technical search and rescue operations in collapsed or failed structures, particularly those involving structures reinforced and/or built with structural steel. They must also conduct rigging and lifting operations. A Heavy USAR team is expected to have the equipment and manpower to work in a technical capacity at two work-sites simultaneously. A second worksite is defined as any area of work that requires a USAR team to re-assign staff and equipment to a different location all of which will require independent logistical support. Generally an assignment of this sort would last greater than 24 hours.

.. note::
 A detailed description, including compositions, of Medium and Heavy USAR teams is outlined in the Volume II, Manual A: Capacity Building.

4.3. INSARAG External Classification (IEC)
============================================

*“Guarantee of effective and professional international assistance.”*

[Figure 7: The INSARAG patch.]

Numerous countries and organisations have successfully undergone the IEC since it started in 2005, while many others have shown keen interest or are preparing their USAR teams for upcoming IECs. This process has since facilitated capacity building and ensured minimum standards and matching capabilities to needs and priorities. IEC teams are well recognised by the INSARAG tag that they wear, and have most recently proven to be a value adding resource to earthquake affected countries such as Indonesia and Haiti, in the immediate aftermath.

To this very day it remains a truly unique process that establishes verifiable operational standards and an example of how independent peer review can provide an added value in preparedness for response, and at the times of response. Both classifiers and the team undergoing IEC learn from one another, and this interaction is indeed highly valuable, because, in an earthquake, they will be the same people working closely together, to help save lives.

To ensure coherence in international USAR response, international teams with the capacity to deploy internationally are strongly encouraged to engage in the IEC process.

4.3.1. Process
------------------

Any USAR team having the mandate to deploy internationally is eligible to apply for an IEC, provided it has the endorsement of its country’s INSARAG Policy Focal Point. Upon successful completion of an IEC, USAR teams are included in the USAR Directory at the classification level achieved.

The IEC assesses and classifies two key components of response and technical capability of international USAR operations:

* Response capability
* Technical capability

USAR teams are required to demonstrate their proficiency during a 36-hour simulated, realistic structural collapse exercise, using their full range of USAR skills and equipment required for the desired classification level. Successful teams are recognised as having met universal USAR standards and are accorded a team patch to identify their professional level in the field. The image at the top of this page is an example of such an identifying patch.

The INSARAG Secretariat facilitates all IEC/Rs and closely engages all teams throughout their respective IEC planning timelines and in close cooperation with their focal points, mentors and IEC Team Leaders.

4.3.2. Cost for IEC/R
-----------------------
All costs associated with the planning, preparation and execution of the IEC/R exercise is the responsibility of the host country or the organisation requesting classification or reclassification.

Many USAR teams have successfully undergone the IEC/R process through close bilateral cooperation among member states. Mentoring and training support are discussed and agreed mutually amongst the stakeholders.

In an IEC/R, the associated costs of individual IEC/R classifiers are covered by their respective sponsoring organisation. The IEC/R requesting host country or the organisation however, determines and manages observers invited. IEC/R teams preparing for their own exercise are usually given priority places for observers.

*INSARAG External Reclassification (IER)*

IER is the process a classified team is required to periodically undergo in order to maintain their classification status. If for whatever reasons a USAR team elects not to reclassify, it surrenders its INSARAG classification.

USAR teams may need to be reclassified for the following reasons:

* Classification period of five years expires
* Change in the USAR team’s structure
* Change in classification level
* Inappropriate international response conduct

Any classified team that is not able to run an IER five years after being classified will have to submit relevant justifications through their Policy Focal Points to the INSARAG Secretariat, who will then consult with the INSARAG Global Chair. In such an instance, their classification status would be considered as pending, contingent on future reclassification.

.. note::
    For more information, please refer to Volume II, Manual C: INSARAG External Classification and Reclassification.

=====================
Conclusion
=====================

The INSARAG Guidelines, prepared by USAR responders and representatives of INSARAG member countries, is recognised by the UN General Assembly Resolution 57/150 as “a flexible and helpful reference tool for disaster preparedness and response efforts.” It is a living document, being improved with the lessons learned from major international USAR operations. It is also the reference document for capacity building at all levels. The Guidelines represent best practice, and all affected and assisting countries are encouraged to actively implement and practise these internationally accepted procedures and to contribute to its development.

I would like to thank all the members of INSARAG who have supported the work of INSARAG since its establishment. We should be proud of what INSARAG has achieved, and we should continue with even greater determination to implement UN General Assembly Resolution 57/150 at all levels worldwide.

Ambassador Toni Frisch

INSARAG Global Chair

February 2015


===================
Annexes
===================

Annex A: Terms of Reference for INSARAG Focal Points
=====================================================

The responsibilities of INSARAG Focal Points can be described as ensuring the efficient information exchange and validation at the appropriate levels in the preparedness and response phases on USAR matters, including capacity building, trainings, policy matters, emergency alerts, requests or acceptance of assistance, mobilisation and provision of international assistance. The responsibilities can be categorised as follows:

    1. Policy: Ensure the promotion of INSARAG Guidelines and methodology within the country and contribute to the continued policy development
    2. Operational: Coordinate the internal information exchange of their own country/organisation with INSARAG during emergencies and strengthen the preparedness both for national and international response

There are also certain administrative responsibilities, such as serving as a point-of-contact between the national government and the INSARAG network, including the Secretariat, the Regional and the Steering Groups

If appropriate, the responsibilities of the policy and operational focal points may also be carried out by the same person.

Responsibilities: INSARAG Policy Focal Point
------------------------------------------------

* Act as focal point on INSARAG policy matters of the government to the INSARAG network, including the Secretariat in OCHA, the respective Regional Group and Chairmanship as well as the INSARAG Steering Group and the Global Chair
* Act as point-of-contact for all national USAR teams – including NGO teams – on INSARAG matters, and be able to endorse the application of national USAR teams for INSARAG External Classifications
* Ensure the promotion and implementation of INSARAG Guidelines and methodology as part of the national disaster management plan and for the national and international response of the country’s USAR teams as defined in UN General Assembly Resolution 57/150 of 16 December 2002 on Strengthening the Effectiveness and Coordination of International USAR Assistance”
* Ensure that relevant information is communicated in a timely manner in times of emergencies to the INSARAG network through the INSARAG Secretariat and/or the relevant channels (i.e. the VO), including on request or acceptance of international assistance
* Represent or ensure representation of the own country at meetings of the respective INSARAG Regional Group, and if applicable the INSARAG Steering Group

Responsibilities: INSARAG Operational Focal Point
----------------------------------------------------

* Act as point-of-contact on INSARAG operational matters for national USAR teams within the country and promote the capacity building of the teams and national disaster management structure in line with INSARAG Guidelines and methodology, including the preparation for the establishment of RDC and OSOCC when required
* When affected by an emergency of international significance within the own country, act as counterpart to the INSARAG Secretariat/OCHA and provide relevant information updates for the international operation in regular intervals to the INSARAG network on the VO
* When responding to an emergency in a third country, act as counterpart to the INSARAG Secretariat/OCHA and provide relevant information updates on the own country’s planned or implemented response in regular intervals to the INSARAG network on the VO

INSARAG Focal Points, whether policy or operational, are also expected to assume administrative responsibilities such as the following:

* Disseminate information from the INSARAG Secretariat, in particular invitations to INSARAG meetings, workshops, training courses or USAR exercises, among relevant disaster management authorities and USAR teams in own country
* Have the capacity to verify or decide whether the own country is prepared support and host INSARAG activities, such as specific workshops, trainings, the annual Team Leaders’ meeting or INSARAG regional exercises.

Annex B: Terms of Reference for the INSARAG Global Chair, Regional Chairs and Vice-Chairs
================================================================================================

Responsibilities: INSARAG Global Chair
---------------------------------------

* Lead the promotion of the INSARAG methodology and guidelines globally amongst countries and organisations and promote participation in all INSARAG bodies
* Lead advocacy on the implementation of the INSARAG Hyogo Declaration and the UN General Assembly Resolution 57/150.
* Chair the annual meeting of the Steering Group
* Actively coordinate the activities of the Steering Group with the Secretariat, including through regular teleconferences and other meetings
* Participate and represent the INSARAG global network in the annual meetings of the other INSARAG bodies (i.e. Regional Group Meetings, Team Leaders Meeting etc.) when available
* Represent the INSARAG network globally in relevant meetings, events and the media

Responsibilities: INSARAG Regional Chairs
--------------------------------------------

* Promote the INSARAG methodology and guidelines amongst countries and organisations of the region and promote their participation in the INSARAG Regional Group, including events like INSARAG regional earthquake response simulation exercises
* Support the implementation of the INSARAG Hyogo Declaration and the UN General Assembly resolution 57/150
* Host and co-organise the annual meeting of the Regional Group, with the support of the INSARAG Secretariat and the Vice Chairs (i.e. preparations for the two-day meeting, logistical arrangements, identifying meeting venue, if possible, covering the costs of accommodation in order to facilitate participation by all countries of the Regional Group)
* Represent the INSARAG network in the region in relevant meetings and events
* Participate and represent the region in the annual INSARAG Steering Group meeting, in February in Geneva, Switzerland
* Actively coordinate the activities of the Regional Group with the Secretariat and the Vice-Chairs, including through regular teleconferences and other meetings
* If possible, participate and represent the region in the annual meetings of the other INSARAG regional groups

INSARAG Regional Vice-Chairs
--------------------------------
* Promote the INSARAG methodology and guidelines amongst countries and organisations of the region and promote their participation in the INSARAG Regional Group, including events like INSARAG regional earthquake response simulation exercises
* Support the implementation of the INSARAG Hyogo Declaration and the UN General Assembly resolution 57/150
* Support the Chair in hosting and co-organising the annual meeting the Regional Group, with the support of the INSARAG Secretariat
* In discussion with the Regional Chair, represent the INSARAG network in the region in relevant meetings and events
* Participate and represent the region in the annual INSARAG Steering Group meeting, in February in Geneva, Switzerland
* Actively coordinate the activities of the Regional Group with the Secretariat and the Chair, including through regular teleconferences and other meetings
* If possible, participate and represent the region in the annual meetings of the other INSARAG regional groups

